# Ettrics Common

Common scripts used by our express applications.

## Ettrics Passport 

Passport configuration and controller

### Configuration

`app` Is your express application.
`schema` Is your sequalize schema. Must have GoogleProfile, FacebookProfile,
TwitterProfile, InstagramProfile schemas.

```javascript
EttricsPassport.configuration({
  app: express,
  schema: express.locals.schema,
  auth: {
    facebook: {
      clientID: config.get('facebook.auth.clientID'),
      clientSecret: config.get('facebook.auth.clientSecret'),
      callbackURL: config.get('facebook.auth.callbackURL'),
      profileFields: ['id', 'emails'] 
    },
    google: {
      clientID: config.get('google.auth.clientID'),
      clientSecret: config.get('google.auth.clientSecret'),
      callbackURL: config.get('google.auth.callbackURL')
    },
    twitter: {
      consumerKey: config.get('twitter.auth.clientID'),
      consumerSecret: config.get('twitter.auth.clientSecret'),
      userProfileURL: "https://api.twitter.com/1.1/account/verify_credentials.json",
      callbackURL: config.get('twitter.auth.callbackURL')
    },
    instagram: {
      clientID: config.get('instagram.auth.clientID'),
      clientSecret: config.get('instagram.auth.clientSecret'),
      callbackURL: config.get('instagram.auth.callbackURL')
    }
  }
});
```
### Controller 

```javascript
// Google Authentication
express.use('/google/auth/connect', PassportController.authConnectGoogle());
express.use('/google/auth/callback', PassportController.authCallbackGoogle(),
  PassportController.authCallbackRedirect);
```

## Ettrics Logger 

Winston logger
