var fs = require('fs');

var libraries = {};

var sourceFiles = fs.readdirSync(__dirname + '/source');

for(var i in sourceFiles){
	var library = require('./source/' + sourceFiles[i]);
	var libraryName = sourceFiles[i];
	libraryName = libraryName[0].toUpperCase() + libraryName.substring(1,libraryName.length - 3);
	libraries[libraryName] = library;
}

module.exports = libraries;