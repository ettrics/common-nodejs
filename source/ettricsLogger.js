'use strict';
var winston = require('winston');

var EttricsLogger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)()
  ]
});
EttricsLogger.level = process.env.LOG_LEVEL || 'warn';

module.exports = EttricsLogger;