'use strict';
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var InstagramStrategy = require('passport-instagram').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;

var Configure = {
  validate: function(config) {
    if(!config.app) {
      throw "Express app has not been provided.";
    }
    else if(!config.schema) {
      throw "Sequalize schema has not been provided.";
    }
    else if(!config.auth.facebook) {
      throw "Facebook auth has not been provided.";
    }
    else if(!config.auth.google) {
      throw "Google auth has not been provided.";
    }
    else if(!config.auth.twitter) {
      throw "Twitter auth has not been provided.";
    }
    else if(!config.auth.instagram) {
      throw "Instagram auth has not been provided.";
    }
  },
  initialize: function(config) {
    config.app.use(passport.initialize());
    config.app.use(passport.session());
  },
  serialize: function() {
    passport.serializeUser(function(user, done) {
      try {
        done(null, user.id);
      }
      catch(err) {
        done(err);
      }
    });
  },
  deserialize: function(config) {
    passport.deserializeUser(function(id, done) {
      config.schema.User.findById(id).then(function(user) {
        done(null, user);
      })
      .catch(function(err) {
        done(err);
      });
    });
  },
  initializePassports: function(config) {
    this.passportLocalRegister(LocalStrategy,
      config.auth.local, config.schema.User);
    this.passportLocalLogin(LocalStrategy,
      config.auth.local, config.schema.User);
    this.passport(FacebookStrategy, config.auth.facebook,
      config.schema.FacebookProfile, config.schema.User);
    this.passport(GoogleStrategy, config.auth.google,
      config.schema.GoogleProfile, config.schema.User);
    this.passport(TwitterStrategy, config.auth.twitter,
      config.schema.TwitterProfile, config.schema.User);
    this.passport(InstagramStrategy, config.auth.instagram,
      config.schema.InstagramProfile, config.schema.User);
  },
  passportLocalRegister: function(Strategy, auth, UserSchema) {
    passport.use('local-register', new Strategy(auth,
      function(email, password, done) {
        return UserSchema.count({
          where: { email: email }
        })
        .then((count) => {
          if(count) {
            return done(null, false, { message: 'Email is registered' });
          }
          else {
            return UserSchema.create({
              email: email,
              password: password
            })
            .then((user) => {
              return done(null, user);
            })
            .catch((error) => {
              return done(null, false, { message: error });
            });
          }
        });
      }
    ));
  },
  passportLocalLogin: function(Strategy, auth, UserSchema) {
    passport.use('local-login', new Strategy(auth,
      function(email, password, done) {
        UserSchema.findOne({
          where: { email: email }
        })
        .then(function(user) {
          if(!user) {
            done(null, false, { message: 'Email is not registered' });
          }
          else if(!user.verifyPassword(password)) {
            done(null, false, { message: 'Invalid password' });
          }
          else {
            done(null, user);
          }
        });
      }
    ));
  },
  passport: function(Strategy, auth, ProfileSchema, UserSchema) {
    passport.use(new Strategy(auth,
      function(accessToken, refreshToken, profile, done) {
        return ProfileSchema.findOrCreateFromProfile(profile, UserSchema)
        .then((profileSchema) => {
          return profileSchema.getUser();
        })
        .then((user) => {
          return done(null, user);
        })
        .catch((err) => {
          done(err);
        });
      }
    ));
  }
};

var Controller = {
  authLocalRegister: function() {
    return passport.authenticate('local-register', {
      failureRedirect: '/auth/failure',
      failureFlash: true
    });
  },
  authLocalLogin: function() {
    return passport.authenticate('local-login', {
      failureRedirect: '/auth/failure',
      failureFlash: true
    });
  },
  authConnectGoogle : function() {
    return passport.authenticate('google', {
      scope: ['email', 'profile']
    });
  },
  authCallbackGoogle : function() {
    return passport.authenticate('google', {
      failureRedirect: '/auth/failure'
    });
  },
  authConnectFacebook : function() {
    return passport.authenticate('facebook', {
      scope: ['email']
    });
  },
  authCallbackFacebook : function() {
    return passport.authenticate('facebook', {
      failureRedirect: '/auth/failure'
    });
  },
  authConnectTwitter : function() {
    return passport.authenticate('twitter');
  },
  authCallbackTwitter : function() {
    return passport.authenticate('twitter', {
      failureRedirect: '/auth/failure'
    });
  },
  authConnectInstagram : function() {
    return passport.authenticate('instagram');
  },
  authCallbackInstagram : function() {
    return passport.authenticate('instagram', {
      failureRedirect: '/auth/failure'
    });
  },
  storeRedirectPage : function(req, res, next) {
    req.session.returnTo = req.originalUrl;
    next();
  },
  authCallbackRedirect : function(req, res) {
    var path = (req.session.returnTo) ? req.session.returnTo : '/';
    res.redirect(path);
  }
};

var EttricsPassport = {
  configuration: function(config) {
    Configure.validate(config);
    Configure.initialize(config);
    Configure.serialize();
    Configure.deserialize(config);
    Configure.initializePassports(config);
  },
  controller: Controller
};

module.exports = EttricsPassport;